<?php
/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 7/14/18
 * Time: 5:15 PM
 */

namespace App\data;


interface Consumer
{
    public function getData();
}