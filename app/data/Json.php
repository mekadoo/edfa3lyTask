<?php
/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 7/14/18
 * Time: 5:14 PM
 */

namespace App\data;

use App\data\Consumer;
use App\Strategies\JsonTransformer;

class Json implements Consumer
{
    /** @var array $data */
    public $data;
    public function __construct()
    {
        $this->data = json_encode([
                ['product_name'=>'laptop','product_price'=>20000],
                ['product_name'=>'PC','product_price'=>2000]
        ]) ;
    }
    /**
     * @return array
     */
    public function getData()
    {
        $type = new JsonTransformer();
        /** @var array $result */
        return $type->transformData($this->data);
    }
}