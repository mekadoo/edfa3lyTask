<?php
/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 7/13/18
 * Time: 8:10 PM
 */

namespace App\Strategies;


use App\Product;
use App\Strategies\Base\BaseTransformer;

class CsvTransformer implements BaseTransformer
{
    public function transformData($data):array
    {
        $products = [];
        foreach ( $data as $product){
            $products[] = ['name'=>$product['product_name'],'price'=>$product["product_price"]];
        }
        return $products;
    }
}