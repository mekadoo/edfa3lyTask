<?php
/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 7/14/18
 * Time: 8:00 PM
 */

namespace App\data;
use App\data\Consumer;
use App\Strategies\CsvTransformer;

class CSV implements  Consumer
{
    public $data;
    public function __construct()
    {
        $this->data = [
            ['product_name'=>'keyboard','product_price'=>400],
            ['product_name'=>'mouse','product_price'=>300]
        ];
    }

    public function getData()
    {
        $type = new CsvTransformer();
        return $type->transformData($this->data);
    }
}