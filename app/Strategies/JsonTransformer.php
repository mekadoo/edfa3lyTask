<?php
/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 7/13/18
 * Time: 7:48 PM
 */

namespace App\Strategies;

use App\Strategies\Base\BaseTransformer;

class JsonTransformer implements BaseTransformer
{
    public function transformData($data):array
    {
        $products = [];
        foreach ( json_decode($data ) as $k => $product){
            $products[] = ['name'=>$product->product_name,'price'=>$product->product_price];
        }
        return $products;
    }
}