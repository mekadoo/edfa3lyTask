<?php
/**
 * Created by PhpStorm.
 * User: Ali
 * Date: 7/13/18
 * Time: 7:47 PM
 */

namespace App\Strategies\Base;

interface BaseTransformer
{
    public function transformData($file):array;
}