
## About The Task

 Create a consumer that process products from different data sources of different data formats and insert them into database (without really inserting anything into the database), where consumer shouldn’t know about external data sources (APIs or files) implementation with the ability to add more data sources without changing code implementation of the consumer
#### tools 
- [Laravel](https://laravel.com/docs).

#### Design Patterns and Solids
- [Strategy Pattern](http://designpatternsphp.readthedocs.io/en/latest/Behavioral/Strategy/README.html)
<p> Why i used Strategy Pattern : One of the dominant strategies of 
    object-oriented design is the "open-closed principle".
    and our case has lot if it may be tommorow we need to import XML files . we didn't want to edit in our 
    logic . also we need to implement (S)solid which is Single responsiblity and (i) interface segregation 
    in my opinion Strategy is the best pettern to implement all of this 
    also we have multiple algorithems for each import type this is the main purpose for strategy   
</p>
<p align="center"> 
<img src="https://imgur.com/a/PCOaWta">
</p>

####
 [If diagram not working ](https://drive.google.com/file/d/1kyyE3M7faUL2CP03SUYtajbnVTY6uT3i/view?usp=sharing).

- **[(S)ingle responsibility ](https://en.wikipedia.org/wiki/Single_responsibility_principle)**
- **[(O)pen Closed](https://en.wikipedia.org/wiki/Open%E2%80%93closed_principle)**
- **[(I)nterface segregation](https://en.wikipedia.org/wiki/Interface_segregation_principle)**